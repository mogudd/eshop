import axios from 'axios';

export function request(config) {
    const instanse = axios.create({
        baseURL: 'https://api.shop.eduwork.cn',
        timeout: 5000,

    })
//请求拦截
    instanse.interceptors.request.use(config => {
        //如果一个接口需要认证才可以访问，就在这统一设置

        //直接放行
        return config
    }, err => {

    })
//响应拦截
    instanse.interceptors.response.use(res => {
            return res.data ? res.data : res;
        }, err => {
            //如果有需要授权才可以访问的接口，统一去 login 授权
            //如果又错误则处理，显示错误信息

        }
    )
    return instanse(config);
}