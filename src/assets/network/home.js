import {request} from './request'

export function getHomeAllData(){
    return request({
        url: '/api/index',
        // method: 'get'
    })
}

/**
 *
 * @param type 默认为'sales' 按销量排行获取
 * @param page 分页 默认 -1
 * @returns {AxiosPromise}
 */
export function getHomeGoods(type = 'sales', page = 1){
return request({
    url:`/api/index?${type}=1&page=${page}`
})
}