import { createRouter, createWebHashHistory } from 'vue-router'
import Home from '../views/Home.vue'
const Category = () => import('@/views/category/category')
const Detail = () => import('@/views/detail/detail')
const home = () => import('@/views/home/home')
const Profile = () => import('@/views/profile/profile')
const Shopcart = () => import('@/views/shopcart/shopcart')

const routes = [
  {
    path: '/',
    name: 'home',
    component: home,
    meta: {
      title: '图书商城'
    }
  },
  {
    path: '/category',
    name: 'Category',
    component: Category,
    meta: {
      title: '分类'
    }
  },
  {
    path: '/detail',
    name: 'Detail',
    component: Detail,
    meta: {
      title: '详情'
    }
  },
  {
    path: '/profile',
    name: 'Profile',
    component: Profile,
    meta: {
      title: '我的'
    }
  },
  {
    path: '/shopcart',
    name: 'Shopcart',
    component: Shopcart,
    meta: {
      title: '购物车'
    }
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})
router.beforeEach((to, from, next) => {
  //如果没有登录， 在这里登录
  next();
  document.title = to.meta.title
})
export default router
